package ru.t1.sochilenkov.tm;

import ru.t1.sochilenkov.tm.constant.ArgumentConstant;
import ru.t1.sochilenkov.tm.constant.CommandConstant;
import ru.t1.sochilenkov.tm.util.FormatUtil;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

import static ru.t1.sochilenkov.tm.util.FormatUtil.formatBytes;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void processCommand(final String command) {
        switch (command) {
            case CommandConstant.VERSION:
                showVersion();
                break;
            case CommandConstant.HELP:
                showHelp();
                break;
            case CommandConstant.ABOUT:
                showAbout();
                break;
            case CommandConstant.INFO:
                showSystemInfo();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.INFO:
                showSystemInfo();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void exit() {
        System.exit(0);
    }


    private static void showErrorArgument() {
        System.out.println("[ERROR]");
        System.out.println("Input program arguments are not correct");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.out.println("[ERROR]");
        System.out.println("Current command is not correct");
    }

    private static void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        System.out.println("PROCESSORS: " + processorCount);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("MAXIMUM MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikita Sochilenkov");
        System.out.println("E-mail: nsochilenkov@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show version info\n", CommandConstant.VERSION, ArgumentConstant.VERSION);
        System.out.printf("%s, %s - Show developer info\n", CommandConstant.ABOUT, ArgumentConstant.ABOUT);
        System.out.printf("%s, %s - Show help info\n", CommandConstant.HELP, ArgumentConstant.HELP);
        System.out.printf("%s, %s - Show system info\n", CommandConstant.INFO, ArgumentConstant.INFO);
        System.out.printf("%s - Close application\n", CommandConstant.EXIT);
    }

}